from pydantic import BaseSettings


class Settings(BaseSettings):
    log_level: str = 'INFO'
    database_dsn: str = 'postgres://cheerful:qwerty123@localhost:5432/test'
    init_db_schemas: bool = True


settings = Settings()
