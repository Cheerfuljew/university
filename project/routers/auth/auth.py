from fastapi.routing import APIRouter
from passlib.context import CryptContext

router = APIRouter()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(db, username: str, password: str):
    pass


@router.post(path='/create')
async def create_user():
    pass

