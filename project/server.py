import logging
import os

from fastapi import FastAPI

from database.client import DBConnect
from settings import settings

from routers.auth import auth


def get_app() -> FastAPI:
    app = FastAPI(debug=True)

    app.include_router(auth.router)

    return app


app = get_app()


@app.on_event('startup')
async def startup_event():
    db = DBConnect(settings.database_dsn)
    await db.start()
    app.state.db = db
    logging.info('Database was connected')

    if settings.init_db_schemas:
        await db.init_schemas(f'{os.getcwd()}/database/schemas')


@app.on_event('shutdown')
async def shutdown_event():
    await app.state.db.close()
