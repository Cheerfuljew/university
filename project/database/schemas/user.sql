do $$ begin
    create type user_role as enum ('user', 'administrator');
EXCEPTION
    when duplicate_object then null;
end $$;


create table if not exists users (
    id serial primary key ,
    first_name varchar (50) not null ,
    last_name varchar (50) not null ,
    created_at timestamptz not null default now(),
    email varchar (100) not null ,
    role user_role not null ,
    hashed_password varchar (100) not null,
    is_active bool not null default true
);