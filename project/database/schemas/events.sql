do $$ begin
    create type event_type as enum ('online', 'offline');
EXCEPTION
    when duplicate_object then null;
end $$;


create table if not exists events (
    id serial primary key ,
    name varchar (100) not null ,
    description text not null ,
    conference_url varchar (255) ,
    creator integer references users (id),
    created_at timestamp not null default now() ,
    started_at timestamp not null
);

create table if not exists online_conference (
    id serial primary key ,
    event_id integer references events (id)
);

create table if not exists online_conference_user (
    online_conference_id integer references online_conference (id),
    user_id integer references users (id),
    connected_at timestamp not null ,
    disconnected_at timestamp
);