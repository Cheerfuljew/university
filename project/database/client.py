import os

import asyncpg
from asyncpg.pool import Pool


class DBConnect:
    def __init__(self, db_uri: str):
        self._db_uri: str = db_uri

        self._pool: Pool | None = None

    async def start(self):
        self._pool = await asyncpg.create_pool(
            dsn=self._db_uri,
            min_size=1,
            max_size=4,
        )

    async def close(self):
        if self._pool is not None:
            await self._pool.close()

    @property
    def pool(self):
        return self._pool

    async def init_schemas(self, schemas_dir: str):
        schemas_list = os.listdir(schemas_dir)
        for schema_name in schemas_list:
            with open(f'{schemas_dir}/{schema_name}') as file:
                await self._pool.execute(file.read())
