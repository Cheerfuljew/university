from enum import Enum
from datetime import datetime

from pydantic import BaseModel, HttpUrl

from .types import DatabaseId, UserId, EventId


class EventType(str, Enum):
    ONLINE = 'online'
    OFFLINE = 'offline'


class EventModel(BaseModel):
    id: DatabaseId
    name: str
    event_type: EventType
    description: str
    conference_url: HttpUrl | None = None
    tags: list | None = None
    creator: UserId
    started_at: datetime
    created_at: datetime

    def is_online(self):
        return EventType.ONLINE == self.event_type

    def is_offline(self):
        return EventType.OFFLINE == self.event_type


class OnlineConferenceModel(BaseModel):
    id: DatabaseId
    event_id: EventId
    visitors: list[UserId]
