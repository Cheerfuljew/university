from datetime import datetime

from pydantic import BaseModel

from .types import DatabaseId, UserId, EventId, ChatRoomId


class ChatRoomModel(BaseModel):
    id: DatabaseId
    event_id: EventId


class ChatMessageModel(BaseModel):
    id: DatabaseId
    chat_room_id: ChatRoomId
    user_id: UserId
    text: str
    created_at: datetime
