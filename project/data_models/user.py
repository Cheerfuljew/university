from enum import Enum
from datetime import datetime


from pydantic import BaseModel, SecretStr, constr, NameEmail
from .types import DatabaseId


class RoleType(str, Enum):
    USER = 'user'
    ADMINISTRATOR = 'administrator'


class BaseUserModel(BaseModel):
    id: DatabaseId
    first_name: constr(max_length=40)
    last_name: constr(max_length=40)
    created_at: datetime
    email: NameEmail
    hashed_password: SecretStr
    role: RoleType

