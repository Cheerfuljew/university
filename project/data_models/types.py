from typing import NewType

DatabaseId = NewType('DatabaseID', int)
UserId = NewType('UserId', int)
EventId = NewType('EventId', int)
ChatRoomId = NewType('ChatRoomId', int)
